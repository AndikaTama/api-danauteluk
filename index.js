var app = require('./shared');
var { Routes } = require('./routes');
var { Server } = require('./server');
var db = require('./database');

Routes.init();
Server.init();

