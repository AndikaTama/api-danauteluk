var app = require('./shared');
var db = require('./database');
var { Articles } = require('./models/Articles');

var errorReturn = { "status" : "503", "errorDesc" : 'Internal Server Error', "data" : null }

class Routes {
    constructor(){}

    init(){
        app.exp.get('/', function (req, res) {
            res.send("Web Service Now Listening on port 9001");
        });

        app.exp.get('/article', function(req, res){
            console.log(req.query);
            db.con.getConnection(function(err) {
                db.con.query("SELECT * FROM danautel_dbbaru.artikel WHERE ?", req.query, function (err, result, fields) {
                    if (err){
                        console.log(err);
                        res.json(errorReturn);
                    } else {
                        console.log(result);
                        res.json({ "status" : "200", "errorDesc" : 'Success', "data" : result });
                    }
                });
            });
        });

        app.exp.get('/articles', function (req, res) {
            db.con.getConnection(function(err) {
                db.con.query("SELECT * FROM danautel_dbbaru.artikel", function (err, result, fields) {
                    if (err){
                        console.log(err);
                        res.json(errorReturn);
                    } else {
                        console.log(result);
                        res.json({ "status" : "200", "errorDesc" : 'Success', "data" : result });
                    }
                });
            });
        });

        app.exp.get('/articles/:id', function (req, res) {
            var reqId = req.params.id;
            db.con.getConnection(function(err) {
                db.con.query("SELECT * FROM danautel_dbbaru.artikel WHERE id=" + reqId, function (err, result, fields) {
                    if (err){
                        console.log(err);
                        res.json(errorReturn);
                    } else {
                        console.log(result);
                        res.json({ "status" : "200", "errorDesc" : 'Success', "data" : result });
                    }
                });
            });
        });

        app.exp.get('/articles/judul/:judul', function (req, res) {
            var reqJudul = req.params.judul;
            db.con.getConnection(function(err) {
                db.con.query("SELECT * FROM danautel_dbbaru.artikel WHERE judul like '%" + reqJudul + "%'", function (err, result, fields) {
                    if (err){
                        console.log(err);
                        res.json(errorReturn);
                    } else {
                        console.log(result);
                        res.json({ "status" : "200", "errorDesc" : 'Success', "data" : result });
                    }
                });
            });
        });

        app.exp.get('/articles/tgl/:tgl', function (req, res) {
            var reqTgl = req.params.tgl;
            db.con.getConnection(function(err) {
                db.con.query("SELECT * FROM danautel_dbbaru.artikel WHERE tgl like '%" + reqTgl + "%'", function (err, result, fields) {
                    if (err){
                        console.log(err);
                        res.json(errorReturn);
                    } else {
                        console.log(result);
                        res.json({ "status" : "200", "errorDesc" : 'Success', "data" : result });
                    }
                });
            });
        });

        app.exp.get('/articles/range/:start/:end', function (req, res) {
            var reqStart = req.params.start;
            var reqEnd = req.params.end;
            db.con.getConnection(function(err) {
                db.con.query("SELECT * FROM danautel_dbbaru.artikel WHERE tgl BETWEEN '" + reqStart + "%' AND '" + reqEnd + "%'", function (err, result, fields) {
                    if (err){
                        console.log(err);
                        res.json(errorReturn);
                    } else {
                        console.log(result);
                        res.json({ "status" : "200", "errorDesc" : 'Success', "data" : result });
                    }
                });
            });
        });
    
    }
}

exports.Routes = new Routes();
